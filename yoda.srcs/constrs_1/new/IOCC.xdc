set_property PACKAGE_PIN U14 [get_ports {LED[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[0]}]

set_property PACKAGE_PIN U15 [get_ports {LED[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[1]}]

set_property PACKAGE_PIN U18 [get_ports {LED[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[2]}]

set_property PACKAGE_PIN U19 [get_ports {LED[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[3]}]

set_property PACKAGE_PIN R19 [get_ports {LED[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[4]}]

set_property PACKAGE_PIN V13 [get_ports {LED[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[5]}]

set_property PACKAGE_PIN P14 [get_ports {LED[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[6]}]

set_property PACKAGE_PIN R14 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[7]}]

set_property PACKAGE_PIN M14 [get_ports {BUTSW[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUTSW[0]}]

set_property PACKAGE_PIN M15 [get_ports {BUTSW[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUTSW[1]}]

set_property PACKAGE_PIN K16 [get_ports {BUTSW[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUTSW[2]}]

set_property PACKAGE_PIN J16 [get_ports {BUTSW[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUTSW[3]}]

set_property PACKAGE_PIN G19 [get_ports {BUTSW[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUTSW[4]}]

set_property PACKAGE_PIN G20 [get_ports {BUTSW[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUTSW[5]}]

set_property PACKAGE_PIN J20 [get_ports {BUTSW[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUTSW[6]}]

set_property PACKAGE_PIN H20 [get_ports {BUTSW[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUTSW[7]}]

set_property PACKAGE_PIN R16 [get_ports {JD[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD[0]}]

set_property PACKAGE_PIN R17 [get_ports {JD[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD[1]}]

set_property PACKAGE_PIN T17 [get_ports {JD[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD[2]}]

set_property PACKAGE_PIN R18 [get_ports {JD[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD[3]}]

set_property PACKAGE_PIN V17 [get_ports {JD[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD[4]}]

set_property PACKAGE_PIN V18 [get_ports {JD[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD[5]}]

set_property PACKAGE_PIN W18 [get_ports {JD[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD[6]}]

set_property PACKAGE_PIN W19 [get_ports {JD[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD[7]}]

set_property PACKAGE_PIN Y18 [get_ports {JK[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JK[0]}]

set_property BEL D5LUT [get_cells {system_i/microblaze_0/U0/MicroBlaze_Core_I/Performance.Core/Decode_I/PreFetch_Buffer_I1/Instruction_Prefetch_Mux[26].Gen_Instr_DFF/Performace_Debug_Control.ex_brki_hit_i_5}]
set_property LOC SLICE_X8Y24 [get_cells {system_i/microblaze_0/U0/MicroBlaze_Core_I/Performance.Core/Decode_I/PreFetch_Buffer_I1/Instruction_Prefetch_Mux[26].Gen_Instr_DFF/Performace_Debug_Control.ex_brki_hit_i_5}]
