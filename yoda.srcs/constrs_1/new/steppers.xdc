## Stepper 0
set_property PACKAGE_PIN Y19 [get_ports {stepper_x[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {stepper_x[0]}]
set_property PACKAGE_PIN W16 [get_ports {stepper_x[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {stepper_x[1]}]
set_property PACKAGE_PIN V16 [get_ports {stepper_x[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {stepper_x[2]}]

set_property PACKAGE_PIN P18 [get_ports {stepper_y[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {stepper_y[0]}]
set_property PACKAGE_PIN P16 [get_ports {stepper_y[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {stepper_y[1]}]
set_property PACKAGE_PIN P16 [get_ports {stepper_y[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {stepper_y[2]}]
