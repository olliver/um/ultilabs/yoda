library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity axi_stepper_driver_v0_9 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here
		reset			: in std_logic;
		pop			: in std_logic;
		enablen			: out std_logic;
		step			: out std_logic;
		direction		: out std_logic;
		interrupt		: out std_logic;
		fifo_wr_en		: out std_logic;
		fifo_dout		: out std_logic_vector(31 downto 0);
		fifo_almost_full	: in std_logic;
		fifo_full		: in std_logic;
		fifo_rd_en		: out std_logic;
		fifo_din		: in std_logic_vector(31 downto 0);
		fifo_almost_empty	: in std_logic;
		fifo_empty		: in std_logic;
		fifo_dcnt		: in std_logic_vector(23 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end axi_stepper_driver_v0_9;

architecture arch_imp of axi_stepper_driver_v0_9 is

	-- component declaration
	component axi_stepper_driver_v0_9_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (
		-- Users to add ports here
		RESET			: in std_logic;
		POP			: in std_logic;
		ENABLEN			: out std_logic;
		STEP			: out std_logic;
		DIRECTION		: out std_logic;
		INTERRUPT		: out std_logic;
		FIFO_WR_EN		: out std_logic;
		FIFO_DIN		: in std_logic_vector(31 downto 0);
		FIFO_ALMOST_FULL	: in std_logic;
		FIFO_FULL		: in std_logic;
		FIFO_RD_EN		: out std_logic;
		FIFO_DOUT		: out std_logic_vector(31 downto 0);
		FIFO_ALMOST_EMPTY	: in std_logic;
		FIFO_EMPTY		: in std_logic;
		FIFO_DCNT		: in std_logic_vector(23 downto 0);
		-- User ports ends

		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component axi_stepper_driver_v0_9_S00_AXI;

begin

-- Instantiation of Axi Bus Interface S00_AXI
axi_stepper_driver_v0_9_S00_AXI_inst : axi_stepper_driver_v0_9_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
		-- Users to add portmaps here
		RESET			=> reset,
		POP			=> pop,
		ENABLEN			=> enablen,
		STEP			=> step,
		DIRECTION		=> direction,
		INTERRUPT		=> interrupt,
		FIFO_WR_EN		=> fifo_wr_en,
		FIFO_DOUT		=> fifo_dout,
		FIFO_ALMOST_FULL	=> fifo_almost_full,
		FIFO_FULL		=> fifo_full,
		FIFO_RD_EN		=> fifo_rd_en,
		FIFO_DIN		=> fifo_din,
		FIFO_ALMOST_EMPTY	=> fifo_almost_empty,
		FIFO_EMPTY		=> fifo_empty,
		FIFO_DCNT		=> fifo_dcnt,
		-- User portmaps ends
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	-- Add user logic here

	-- User logic ends

end arch_imp;
