library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stepper_driver is
	port (
		debug_out			: out std_logic_vector(31 downto 0); -- TODO rename to a proper register param
		aclk				: in std_logic;
		aresetn				: in std_logic;
		stepper_ctrl			: in std_logic_vector(31 downto 0);
		stepper_status			: out std_logic_vector(31 downto 0);
		stepper_output_pulse_width	: in std_logic_vector(31 downto 0);
		stepper_steps_in		: in std_logic_vector(31 downto 0); -- period?
		stepper_steps_in_load		: in std_logic;
		reset				: in std_logic;
		pop				: in std_logic;
		enablen				: out std_logic;
		step				: out std_logic;
		direction			: out std_logic;
		interrupt			: out std_logic;
		fifo_wr_en			: out std_logic;
		fifo_dout			: out std_logic_vector(31 downto 0);
		fifo_almost_full		: in std_logic;
		fifo_full			: in std_logic;
		fifo_rd_en			: out std_logic;
		fifo_din			: in std_logic_vector(31 downto 0);
		fifo_almost_empty		: in std_logic;
		fifo_empty			: in std_logic;
		fifo_dcnt			: in std_logic_vector(23 downto 0)
	);
end stepper_driver;

architecture behavioral of stepper_driver is

	signal out_buffer_cnt_i		: natural range 0 to 15;
	signal interrupt_i		: std_logic;
	signal out_buffer_i		: std_logic_vector(31 downto 0);
	signal stepper_steps_in_i	: std_logic_vector(31 downto 0);
	signal step_i			: std_logic;
	signal dir_i			: std_logic;
	signal output_pulse_width_i	: natural;
	signal output_pulse_width_run_i	: std_logic;
	signal fifo_almost_full_i	: std_logic;
	signal fifo_full_i		: std_logic;
	signal fifo_almost_empty_i	: std_logic;
	signal fifo_empty_i		: std_logic;
	alias stepper_enable		: std_logic is stepper_ctrl(0);
	alias stepper_dir_inv		: std_logic is stepper_ctrl(1);
	alias stepper_irq_clr		: std_logic is stepper_ctrl(2);
	alias fifo_external		: std_logic is stepper_ctrl(3);
	alias output_pulse_width	: std_logic_vector is stepper_output_pulse_width;
begin

	process (aclk) is
	begin
		if (rising_edge(aclk)) then
			if (fifo_external = '1') then
				-- When running with an external fifo, its status is always used.
				fifo_almost_full_i <= fifo_almost_full;
				fifo_full_i <= fifo_full;
				fifo_almost_empty_i <= fifo_almost_empty;
				fifo_empty_i <= fifo_empty;
			end if;

			-- TODO, if all fifo lines are tied to GND, use internal buffer, else external fifo.
			if ((aresetn = '0') or (reset = '1')) then
				-- Reset state, ensure everything is at default values during reset.
				stepper_steps_in_i <= (others => '0');
				out_buffer_i <= (others => '0');
				out_buffer_cnt_i <= 0;
				interrupt_i <= '0';
				output_pulse_width_i <= 0;
				step_i <= '0';
				dir_i <= '0';
				output_pulse_width_run_i <= '0';

				if (fifo_external = '0') then
					-- Internal fifo is of size '1' and thus it is always either empty, or full. We start with an empty buffer.
					fifo_almost_full_i <= '1';
					fifo_full_i <= '0';
					fifo_almost_empty_i <= '0';
					fifo_empty_i <= '1';
				end if;
			else
				if (stepper_steps_in_load = '1') then
					-- Whenever new data is put onto the register, copy it into the buffer or fifo
					if (fifo_external = '1') then
						fifo_dout <= stepper_steps_in;
						fifo_wr_en <= '1';
					else
						stepper_steps_in_i <= stepper_steps_in;
						fifo_almost_full_i <= '0';
						fifo_full_i <= '1';
						fifo_almost_empty_i <= '1';
						fifo_empty_i <= '0';
					end if;
					debug_out <= stepper_steps_in;
				else
					null;
				end if;

				if (pop = '1') then
					-- Output 1 step during each pop
					step_i <= out_buffer_i(0);
					dir_i <= out_buffer_i(1);
					out_buffer_i(31 downto 0) <= b"00" & out_buffer_i(31 downto 2); -- right shift by 2

					-- Fifo is 32 bit wide, our counter is thus half of that
					if (out_buffer_cnt_i = 15) then
						out_buffer_cnt_i <= 0;
					else
						out_buffer_cnt_i <= out_buffer_cnt_i + 1;
					end if;

					-- Start counting the pulse with after a pop. Note, a pulse width should not be wider then the pop frequency!
					output_pulse_width_run_i <= '1';
				else
					-- When not outputting a step, and all steps have been outputted, load a new register into the out buffer
					if (out_buffer_cnt_i = 0) then
						if (fifo_empty_i = '0') then
							if (fifo_external = '0') then
								-- Get new stepper data from internal buffer in the processing buffer
								-- and mark buffer as available.
								out_buffer_i <= stepper_steps_in_i;

								fifo_almost_full_i <= '1';
								fifo_full_i <= '0';
								fifo_almost_empty_i <= '0';
								fifo_empty_i <= '1';
							else
								-- Get new stepper data from fifo in the processing buffer
								fifo_rd_en <= '1';
								out_buffer_i <= fifo_din;
							end if;
						else
							-- The buffer should never be empty. Make sure no movement happens in the processing buffer
							-- and raise interrupt.
							out_buffer_i <= (others => '0');
							interrupt_i <= '1'; -- ERROR, buffer empty (TODO -> status) and clear interrupt flag on reading of the IRQ in status.
						end if;
					else
						null; -- TODO what to do otherwise?
					end if;
				end if;

				if (output_pulse_width_run_i = '1') then
					if (output_pulse_width_i >= to_integer(unsigned(output_pulse_width))) then
						output_pulse_width_i <= 0;
						step_i <= '0';
						dir_i <= '0';
						output_pulse_width_run_i <= '0';
					else
						output_pulse_width_i <= output_pulse_width_i + 1;
					end if;
				end if;
			end if;
		end if;
	end process;

	enablen <= (not stepper_enable);
	step <= step_i;		-- Even bits in stream are steps
	direction <= dir_i;	-- Odd bits in stream are direction
	interrupt <= interrupt_i;
	stepper_status <= (0 => fifo_full_i, 1 => fifo_empty_i, 2 => fifo_almost_full_i, 3 => fifo_almost_empty_i, others => '0');

end behavioral;
