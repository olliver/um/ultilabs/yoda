library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock_generator is
	port (
		aclk		: in std_logic;
		aresetn		: in std_logic;
		clock_ctrl	: in std_logic_vector(31 downto 0);
		clock_cnt	: in std_logic_vector(31 downto 0);
		clock_mask	: in std_logic_vector(31 downto 0);
		clock_toggle	: in std_logic_vector(31 downto 0);
		clock_tick_mask	: in std_logic_vector(31 downto 0);
		clock_out	: out std_logic_vector(31 downto 0);
		ext_clock_in	: in std_logic
	);
end clock_generator;

architecture behavioral of clock_generator is

	alias clk_en		: std_logic is clock_ctrl(0);	--! Clock output enable
	alias clk_src		: std_logic_vector(1 downto 0) is clock_ctrl(2 downto 1);	--! Clock source select; 0b00 (default) internal clock. 0b01 use external clock pin. 0b10 use manual clk_tick register bit.

	alias clk_tick		: std_logic is clock_ctrl(31);
	alias clk_tick_mask	: std_logic_vector is clock_tick_mask;

	alias clk_cnt		: std_logic_vector is clock_cnt;		--! Divide clock by n count.
	alias clk_mask		: std_logic_vector is clock_mask;		--! Clock output mask, bit n masks output n.
	alias clk_toggle	: std_logic_vector is clock_toggle;		--! Clock output toggle/invert bit

	signal clk_out_i	: std_logic_vector(31 downto 0);
	signal clk_cnt_i	: natural;

begin
	process (aclk) is
	begin
		if (rising_edge(aclk)) then
			if (aresetn = '0' or clk_en = '0') then
				clk_cnt_i <= 0;
				clk_out_i <= (others => '0');
			else
				if (clk_cnt_i >= to_integer(unsigned(clk_cnt))) then
					clk_out_i <= (others => '1');
					clk_cnt_i <= 0;
				else
					clk_out_i <= (others => '0');
					case (clk_src) is
					when (b"01") =>
						if (ext_clock_in = '1') then
							clk_cnt_i <= clk_cnt_i + 1;
						end if;
					when (b"10") => -- Better, but also let us mask ticks??
						if (clk_tick = '1') then
						-- 	clk_tick <= '0'; -- TODO multidriven net error? slv_reg0 input only?
							clk_cnt_i <= to_integer(unsigned(clk_cnt)); -- overflow count/set to max.
						end if;
					when others =>
						clk_cnt_i <= clk_cnt_i + 1;
					end case;
				end if;
			end if;
		end if;
	end process;

	clock_out <= (clk_out_i and clk_mask) xor clk_toggle;

end behavioral;
